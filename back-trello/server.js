const express = require("express")
const cors = require('cors')
const app = express()
const fs = require('fs')

app.use(cors())

app.post('/create', (req, res) => {
  console.log(req.body)
  fs.writeFile('data.json', JSON.stringify(req.body), 'utf8', (err) => {
    console.log('eba')
  })
})

app.get('/', (req, res) => {
  fs.readFile('data.json', 'utf8', (err, data) => {
    const obj = JSON.parse(data)
    res.send(obj)
  })
})


app.listen(3001, () => {
  console.log("Server running on port 3001")
})

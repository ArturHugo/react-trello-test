import data from './data.json'
import React from "react";
import Board from "react-trello";
import api from "../../api"

export default class App extends React.Component {
  state = {
    data: data
  }
  componentDidMount(){
    api.get('/').then((res) => {
      // console.log(res.data)
      this.setState({data: res.data})
    })
  }

  WriteData = (newData) => {
    console.log(newData)
    api.post('/create', newData)
  }

  render() {
    return (
      <Board
        data={this.state.data}
        draggable
        editable
        onDataChange={this.WriteData}
      />
    );
  }
}

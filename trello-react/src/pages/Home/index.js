import React, { Component } from "react"
import Navbar from "../../components/Navbar"
import Kanban from "../../components/Kanban"

import './style.css'

export default class Home extends Component {
  render() {
    return (
      <div className="backgroundImage">
        <Navbar />
        <h1>Melhor que o trello!!</h1>
        <Kanban />
      </div>
    );
  }
}
